api = 2
core = 7.87
projects[drupal][type] = core


;Core Modules
projects[acl][version] = 1.1
projects[ctools][version] = 1.14
projects[features][version] = 2.10
projects[date][version] = 2.10
projects[date_ical][version] = 3.9
projects[diff][version] = 3.3
projects[entity][version] = 1.9
projects[entityreference][version] = 1.5
projects[entityreference_filter][version] = 1.7
projects[entityreference_prepopulate][version] = 1.7
projects[email][version] = 1.3
projects[redirect][version] = 1.0-rc3
projects[rules][version] = 2.13
projects[stringoverrides][version] = 1.8
projects[strongarm][version] = 2.0
projects[token][version] = 1.7
projects[panels][version] = 3.9
projects[pathauto][version] = 1.3
projects[migrate][version] = 2.9
projects[jquery_update][version] = 2.7
projects[libraries][version] = 2.3
projects[google_analytics][version] = 2.4
projects[honeypot][version] = 1.24
projects[advagg][version] = 2.33
projects[filefield_nginx_progress][version] = 2.3


;Admin Modules
projects[admin_menu][version] = 3.0-rc5
projects[admin_menu_navbar][version] = 1.0
projects[adminimal_admin_menu][version] = 1.9
projects[devel][version] = 1.7
projects[backup_migrate][version] = 3.5

;Media Modules
projects[audiofield][version] = 1.4
projects[auto_nodetitle][version] = 1.0
projects[breadcrumbs_by_path][version] = 1.0-alpha13
projects[calendar][version] = 3.5


;Views Modules
projects[views][version] = 3.18
projects[views_bootstrap][version] = 3.2
projects[views_bulk_operations][version] = 3.6
projects[views_responsive_grid][version] = 1.3
projects[draggableviews][version] = 2.1


;Extras
projects[captcha][version] = 1.5
projects[content_access][version] = 1.2-beta2
projects[context][version] = 3.7
projects[customfilter][version] = 1.0
projects[fb_likebox][version] = 1.10
projects[fboauth][version] = 2.0-rc3
projects[field_collection][version] = 1.0-beta12
projects[field_collection_table][version] = 1.0-beta5
projects[imagecache_actions][version] = 1.9
projects[markdown][version] = 1.5
projects[menu_block][version] = 2.7
projects[menu_trail_by_path][version] = 3.3
projects[mobile_detect][version] = 1.0-alpha1
projects[module_filter][version] = 2.1
projects[mollom][version] = 2.15
projects[navbar][version] = 1.7
projects[nodeorder][version] = 1.5
projects[recaptcha][version] = 2.2
projects[wordpress_migrate][version] = 2.3
projects[fpa][version] = 2.6

;Theme related
projects[tao][version] = 3.1
projects[rubik][version] = 4.4
projects[bootstrap][version] = 3.7
projects[panels_bootstrap_layouts][version] = 3.0
projects[bootstrap_carousel][version] = 1.2
projects[bootstrap_core][version] = 3.x-dev
projects[fontawesome][version] = 2.7


;Libraries
libraries[fontawesome][download][type] = get
libraries[fontawesome][download][url] = https://use.fontawesome.com/releases/v5.0.1/fontawesome-free-5.0.1.zip
libraries[fontawesome][destination] = libraries
